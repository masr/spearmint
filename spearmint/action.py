#!/usr/bin/env python3

from types import FunctionType, BuiltinFunctionType
import subprocess
import ijson


class Action:
    def __init__(self, function, condition=None):
        self.function = function
        self.name = function.__name__
        self.condition = condition

    def run(self, **kwargs):
        """Perform action"""
        result = self.function(**kwargs)
        return result

    def step(self, i, **kwargs):
        if not self.condition or self.condition(kwargs):
            return Step(self, i, kwargs)


class ProcessAction(Action):
    def __init__(
        self, command, parser=ijson.items, encoding="utf-8", condition=None, **kwargs
    ):
        """kwargs are passed to subprocess; e.g. using shell=True to process a single string via a shell"""
        self.command = command
        self.parser = parser
        self.encoding = encoding
        self.name = command[0]
        self.condition = condition
        self.kwargs = kwargs

    def run(self, **kwargs):
        """Run a process, passing arguments via ENV"""
        proc = subprocess.run(
            self.command,
            env=kwargs,
            encoding=self.encoding,
            capture_output=True,
            **self.kwargs,
        )
        if proc.return_code == 0:
            return self.parser(proc.stdout)
        else:
            raise Exception(proc.stderr)


class Step:
    def __init__(self, action: Action, index, params: dict):
        self.action = action
        self.index = index
        self.params = params

    def run(self):
        return self.action.run(**self.params)

    def __str__(self):
        return f"{self.index}:{self.action.name}({self.params})"


def factory(action, condition=None):
    if isinstance(action, Action):
        return action
    elif isinstance(action, str):
        # Generally speaking programs do not have spaces in their names
        return ProcessAction(action.split(" "), condition=condition)
    elif isinstance(action, list):
        return ProcessAction(action, condition=condition)
    elif isinstance(action, (FunctionType, BuiltinFunctionType)):
        return Action(action, condition=condition)
