#!/usr/bin/env python3

import itertools
import json
import ijson
import os
import pandas as pd
import time
from datetime import datetime
from spearmint import action


class Experiment:
    def __init__(
        self,
        name=None,
        parameters=None,
        actions=[],
        timestamp_key="timestamp",
        duration_key="duration",
        log_dir="logs",
        preprocessors=[],
        postprocessors=[],
    ):
        self.name = name
        self.parameters = parameters or {}
        self.actions = [action.factory(a) for a in actions]
        self.timestamp_key = timestamp_key
        self.duration_key = duration_key
        self.log_dir = log_dir
        self.results = []
        self.keys = []
        self.preprocessors = preprocessors
        self.postprocessors = postprocessors

    def run(self, *, start=0, end=None, verbose=False):
        """Run the experiment"""

        start_time = datetime.now()
        timestamp = start_time.strftime("%Y%m%d-%H%M%S")
        filename = f"{self.name}-{timestamp}.log"
        path = os.sep.join([self.log_dir, filename])
        os.makedirs(os.path.dirname(path), exist_ok=True)
        zero = time.perf_counter()

        with open(path, "w") as f:
            if verbose:
                print(f"logging results to {path}")
            for i, step in itertools.islice(enumerate(self.plan()), start, end):
                if verbose:
                    print(f"running step {i}: {step.params}")
                result = step.run()
                if result:
                    if isinstance(result, dict):
                        timestamp = time.perf_counter() - zero
                        result[self.timestamp_key] = timestamp
                        # need to append one entry to keys for every result
                        self.keys.append(step.params)
                        self.results.append(result)
                        if verbose:
                            print(f"result: {result}")
                        f.writelines(json.dumps({**step.params, **result}) + "\n")
                    else:  # iterator
                        for r in result:
                            timestamp = time.perf_counter() - zero
                            r[self.timestamp_key] = timestamp
                            # need to append one entry to keys for every result
                            self.keys.append(step.params)
                            self.results.append(r)
                            if verbose:
                                print(f"result: {r}")
                            f.writelines(json.dumps({**step.params, **r}) + "\n")

        if self.results:
            return self.process_results()
        else:
            print(f"no results; removing {path}")
            os.remove(path)

    def process_results(self):
        for p in self.preprocessors:
            p(self)
        index = pd.MultiIndex.from_frame(pd.DataFrame(self.keys))
        df = pd.DataFrame(self.results, index=index)
        df[self.duration_key] = (
            df[self.timestamp_key].diff(1).fillna(df[self.timestamp_key])
        )
        for p in self.postprocessors:
            df = p(df)
        self.frame = df
        return df

    def clear(self):
        self.keys = []
        self.results = []
        return self

    def from_log(self, path):
        return self.clear().load_log(path)

    def load_log(self, path):
        with open(path, "r") as f:
            for o in ijson.items(f, "", multiple_values=True):
                key = {}
                result = {}
                for k in o:
                    if k in self.parameters:
                        key[k] = o[k]
                    else:
                        result[k] = o[k]
                self.keys.append(key)
                self.results.append(result)
        return self.process_results()

    def iterations(self):
        """Generate all combinations of parameter values"""
        values = itertools.product(*self.parameters.values())
        for v in values:
            yield dict(zip(self.parameters.keys(), v))

    def plan(self):
        """Generate a plan from the experiment as a sequence of steps"""
        for i, params in enumerate(self.iterations()):
            for a in self.actions:
                s = a.step(i, **params)
                if s:
                    yield s

    def action(self, *args, **kwargs):
        """Decorator for adding an action to the experiment"""
        if args:
            fn = args[0]
            self.actions.append(action.factory(fn))
        else:
            # assemble condition from kwargs
            filters = kwargs

            def condition(parameters):
                for k in filters:
                    if k not in parameters:
                        return False
                    if callable(filters[k]):
                        if not filters[k](parameters[k]):
                            return False
                    elif isinstance(filters[k], (list, tuple, set)):
                        if parameters[k] not in filters[k]:
                            return False
                    elif filters[k] != parameters[k]:
                        return False
                return True

            def wrapper(fn):
                self.actions.append(action.factory(fn, condition))

            return wrapper

    def preprocess(self, fn):
        """
        Register a function to run before the final DataFrame loading occurs.

        Useful for cleaning up data so it's compatible with DataFrames, and removing unneeded keys.

        Interface: fn(Experiment) -> None
        Takes a single Experiment object and mutates it directly.
        """
        self.preprocessors.append(fn)

    def postprocess(self, fn):
        """
        Register a function for processing the DataFrame before the results are finally returned.

        Interface: fn(DataFrame) -> DataFrame
        Takes the old DataFrame and returns a new one.
        The final result after all postprocessors are run is saved as Experiment.frame
        """
        self.postprocessors.append(fn)

    def command(self, name):
        """
        A decorator for adding commands to the experiment object, for CLI usage

        Usage:
          @experiment.command("test")
          def test(args):
            ...do stuff...

        CLI usage:
          python experiment test [args]
        """

        def wrapper(fn):
            setattr(self, name, fn)

        return wrapper
