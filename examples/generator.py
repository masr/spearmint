#!/usr/bin/env python3

from spearmint import Experiment
import fire
import time
import random

experiment = Experiment(
    name="generator",
    parameters={"iteration": range(5), "root": range(5)},
)


@experiment.action
def process(iteration=None, root=None):
    val = root
    for i in range(root):
        val *= root
        time.sleep(random.random() / 1000)
        yield {"val": val}


if __name__ == "__main__":
    fire.Fire(experiment)
