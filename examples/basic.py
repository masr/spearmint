#!/usr/bin/env python3

from spearmint import Experiment
import fire

experiment = Experiment(
    name="basic",
    parameters={"iteration": range(10), "root": range(5)},
)


@experiment.action
def process(iteration=None, root=None):
    val = root
    for i in range(root):
        val *= root
    return {"val": val}


if __name__ == "__main__":
    fire.Fire(experiment)
