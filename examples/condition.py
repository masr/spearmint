#!/usr/bin/env python3

from spearmint import Experiment
import fire

experiment = Experiment(
    name="condition",
    parameters={"iteration": range(2), "root": range(10)},
)

# restrict an action to only apply when parameters match a specific value
@experiment.action(root=9)
def process(iteration=None, root=None):
    val = root
    return {"val": val, "type": "nine"}


# or match one in a list/tuple/set of values
@experiment.action(root=(1, 3, 5, 7))
def process(iteration=None, root=None):
    val = root
    return {"val": val, "type": "prime"}


# or pass a filter function
@experiment.action(root=lambda x: x % 2 == 0)
def process(iteration=None, root=None):
    val = root
    return {"val": val, "type": "even"}


if __name__ == "__main__":
    print(experiment.run().to_string())
